import random
pchoice = 0
def tictactoe_board(value):
    print("\t _________________")
    print("\t|     |     |     |")
    print("\t|  {}  |  {}  |  {}  |".format(value[6], value[7], value[8]))
    print("\t|_____|_____|_____|")
    print("\t|     |     |     |")
    print("\t|  {}  |  {}  |  {}  |".format(value[3], value[4], value[5]))
    print("\t|_____|_____|_____|")
    print("\t|     |     |     |")
    print("\t|  {}  |  {}  |  {}  |".format(value[0], value[1], value[2]))
    print("\t|_____|_____|_____|")
    print("\n")

 

def scoreboard(score_board):
    print("\t ________________________________")
    print("\t|                                |")
    print("\t|             SKÓRE              |")
    print("\t|________________________________|")
    print("\t|                                |")
 
    player = list(score_board.keys())
    print("\t|\t   ", player[0], "\t    ", score_board[player[0]],"  |")
    print("\t|\t   ", player[1], "\t    ", score_board[player[1]],"  |")
 
    print("\t|________________________________|\n")

def win(position, cur_player):
 

    winning_combinations = [[1, 2, 3], [4, 5, 6], [7, 8, 9], [1, 4, 7], [2, 5, 8], [3, 6, 9], [1, 5, 9], [3, 5, 7]]
 

    for x in winning_combinations:
        if all(y in position[cur_player] for y in x):
            return True      
    return False       
 
def draw(position):
    if len(position['X']) + len(position['O']) == 9:
        return True
    return False       

def computer_move(value):
    options = [i for i, x in enumerate(value) if x == ' ']
    move = random.choice(options)
    return move + 1
 
 
def whole_game():
    global pchoice
    print("\n")
    print("Hráč 1") 
    player1 = input("Zadej jméno: ")
    print("\n")

    print("Vyber si svého soupeře: ")
    print("Stiskni 1 pro hru proti druhému hráči.")
    print("Stiskni 2 pro hru proti počítači.")
    opponent = int(input())

    if opponent == 1:
        pchoice += 1
        print("\n")
        print("Hráč 2")
        player2 = input("Zadej jméno: ")
        print("\n")
        cur_player = player1
        score_board = {player1: 0, player2: 0}
    
    else:
        player2 = "Počítač"
        cur_player = player1
        score_board = {player1: 0, player2: 0}

    round_number = 1

    player_choice = {'X' : "", 'O' : ""}
    options = ['X', 'O']
 

    score_board = {player1: 0, player2: 0}
    scoreboard(score_board)
 

    while True:
 

        print("Začíná", cur_player)
        print("Zadejte 1 pro začátek hry.")
        print("Zadejte 2 pro konec hry.")
        
 

        try:
            choice = int(input())   
        except ValueError:
            print("Nesprávný vstup! Stiskni 1 nebo 2.\n")
            continue
 
 
        if choice == 1:
            player_choice['X'] = cur_player
            if cur_player == player1:
                player_choice['O'] = player2
            else:
                player_choice['O'] = player1
 
        elif choice == 2:
            print("Konec hry!")
            break  

        else:
            print("Nesprávný vstup! Stiskni 1 nebo 2.\n")
            continue
 

        winner = tictactoe(options[choice-1], round_number)
         

        if winner != 'Remíza!':
            player_won = player_choice[winner]
            score_board[player_won] = score_board[player_won] + 1
 
        scoreboard(score_board)

        if round_number % 2 == 0:
            if cur_player == player1:
                cur_player = player2
            else:
                cur_player = player1
        else:
            if cur_player == player2:
                cur_player = player1
            else:
                cur_player = player2

        round_number += 1


def tictactoe(cur_player, round_number):
    global pchoice

    value = [' ' for x in range(9)]
    position = {'X':[], 'O':[]}

    while True:
        tictactoe_board(value)
        
        if pchoice == 0:
            if round_number % 2 == 1:
                if cur_player == 'O':
                    move = computer_move(value)
                    print("Počítač si vybral pole {}.".format(move))
                else:
                    try:
                        print("Hráč ", cur_player, " je na řadě. Vyberte pole: ", end="")
                        move = int(input()) 
                    except ValueError:
                        print("Špatný vstup! Zadejte číslo.")
                        continue
            elif round_number % 2 == 0:
                if cur_player == 'X':
                    move = computer_move(value)
                    print("Počítač si vybral pole {}.".format(move))
                else:
                    try:
                        print("Hráč ", cur_player, " je na řadě. Vyberte pole: ", end="")
                        move = int(input()) 
                    except ValueError:
                        print("Špatný vstup! Zadejte číslo.")
                        continue
        else:
            try:
                print("Hráč ", cur_player, " je na řadě. Vyberte pole: ", end="")
                move = int(input()) 
            except ValueError:
                print("Špatný vstup! Zadejte číslo.")
                continue
        
        if move < 1 or move > 9:
            print("Špatný vstup! Zkus jiný.")
            continue
 

        if value[move-1] != ' ':
            print("Pole už je obsazeno. Zkus jiné!")
            continue
 
        value[move-1] = cur_player
 

        position[cur_player].append(move)
 

        if win(position, cur_player):
            tictactoe_board(value)
            print("Hráč ", cur_player, " vyhrál!")     
            return cur_player

        if draw(position):
            tictactoe_board(value)
            print("Remíza!")
            return 'Remíza!'
 
        
        if cur_player == 'X':
            cur_player = 'O'
        else:
            cur_player = 'X'
whole_game()